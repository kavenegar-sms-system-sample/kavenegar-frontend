# Kavenegar Front-End

This is a **MERN** project that we are going to learn about **Kavenegar SMS System**.

## Techs

### Front-End

Front-End is a **Reactjs** application.

### Back-End

API is created with **ExpressJs**.

[Link to ExpressJs](https://gitlab.com/kavenegar-sms-system-sample/kavenegar-api)

### Database

We use **MongoDB** as our database.

## How to run

### First clone and install deps

```bash
$ git clon https://gitlab.com/kavenegar-sms-system-sample/kavenegar-frontend && cd kavenegar-frontend
$ npm i
```

### Then run application

```bash
$ npm start
```