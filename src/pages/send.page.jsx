import { useState } from "react";

import {
    Container,
    Card,
    CardHeader,
    CardContent,
    TextField,
    Button,
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    FormControlLabel,
} from "@mui/material";

import Axios from "axios";

const smsServices = [
    {
        value: "kavenegar",
        label: "Kavenegar",
        disable: true,
    },
    {
        value: "payamsms",
        label: "PayamSMS",
        disable: false,
    },
    {
        value: "magfa",
        label: "MagFa",
        disable: true,
    },
];

const SendPage = () => {
    const env = process.env;

    const [receptor, setReceptor] = useState('98');
    const [message, setMessage] = useState("");

    const [gateway, setGateway] = useState("payamsms");

    const [loading, setLoading] = useState(false);

    const sendSms = () => {
        setLoading(true);

        const sendData = {
            messages: [
                {
                    body: message,
                    recipient: receptor,
                }
            ],
        }

        Axios.post(`${env.REACT_APP_BACKEND_URL}/v1/sms/payamsms/send`, sendData)
            .then((result) => {
                setLoading(false);

                setMessage("");

                alert("Message sent");
            })
            .catch((error) => {
                setLoading(false);

                setMessage("");

                alert(error.response.data.message);
            });
    }

    return (
        <Container
            maxWidth="sm"
        >
            <Card
                variant="elevation"
                elevation={20}
                sx={{
                    borderRadius: 1,
                }}
            >
                <CardHeader
                    title="SMS Service"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                />
                <CardContent>
                    <TextField
                        variant="outlined"
                        label="Receptor"
                        placeholder="Enter receptor ( Eg: 98901xxxxxxx )"
                        margin="normal"
                        value={receptor}
                        onChange={(e) => setReceptor(e.target.value)}
                        fullWidth
                    />
                    <TextField
                        variant="outlined"
                        label="Message"
                        placeholder="Enter what you want to send"
                        margin="normal"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                        rows={5}
                        multiline
                        fullWidth
                    />
                    <FormControl margin="normal">
                        <FormLabel>Gateway</FormLabel>
                        <RadioGroup
                            value={gateway}
                            onChange={(e) => setGateway(e.target.value)}
                            row
                        >
                            {
                                smsServices.map((service, index) => (
                                    <FormControlLabel
                                        key={service.label + ' ' + index}
                                        value={service.value}
                                        control={<Radio />}
                                        label={service.label}
                                        disabled={service.disable}
                                    />
                                ))
                            }
                        </RadioGroup>
                    </FormControl>
                    <Button
                        variant="contained"
                        onClick={() => !loading && sendSms()}
                        sx={{
                            mt: 1
                        }}
                        disabled={loading && true}
                        disableElevation
                        fullWidth
                    >
                        { loading ? "Wait" : "Continue" }
                    </Button>
                </CardContent>
            </Card>
        </Container>
    );
}

export default SendPage;